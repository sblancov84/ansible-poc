#!/bin/bash

apt-get update
DEBIAN_FRONTEND=nointeractive apt-get upgrade -y
apt-get install -y python3-pip

pip3 install ansible
