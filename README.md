# Ansible PoC

## Goal

I want to learn how to use Ansible. Basic level, not a complicated thing.

## Structure

This project is composed of two virtual machines managed by Vagrant:

- Alice is the control node machine, where ansible is installed.
- Bob is a managed node machine which is managed by Alice.

Their operating system is Debian 10.

The provision script has all dependencies they need. Alice need python3 and Ansible, just that. Bob will be managed by Alice, so provision of Bob will be done by Alice.

## Concepts

### Configuration

Ansible has a configuration file called "ansible.cfg" and it should be placed where ansible will be called. there are more places where it can be placed but this is a basic example.

### Inventory

This file is used to specify which machines are managed by ansible. In this example, we have an inventory directory where there is an "all" file which contains all machines we will manage (just one: Bob).

### Playbook

Playbook describes the state that machine should have. This example is very simple, there are lots of states it can be described.

## Requirements

- Linux (Debian10.2 tested)
- Vagrant (v2.2.5 tested)
- VirtualBox (v6.0.14 tested) or other provider
